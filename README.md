# contiki-ng with LoRa

This repository contain a base project to use contiki with a
[LoRa shield](https://github.com/tperale/zolertia-lora-breakout)

This repository is based on a [fork of contiki-ng](https://github.com/tperale/contiki-ng) 
that allow to use longer timeslot with TSCH.

## Usage

In the `/src` folder compile the `coordinator` code and `node` code
respectively with.

```
> make PORT=/dev/ttyUSB<x> coordinator.upload
> make PORT=/dev/ttyUSB<x> node.upload
```

The example is by default compiled with _TSCH_ as MAC layer.
